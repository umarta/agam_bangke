<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="kimiamania" />

	<title>LOKER </title>
    
	<link rel="stylesheet" type="text/css" href="../design/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../design/css/style.css">
    <link rel="stylesheet" type="text/css" href="../design/css/font-awesome.css">
    
    <!-- DINAMIK TABEL -->
    <link rel="stylesheet" href="../design/css/demo_page.css" />
    <link rel="stylesheet" href="../design/css/demo_table.css" />
    <link rel="stylesheet" href="../design/css/DT_bootstrap.css" />
    <link href="../design/css/style2.css" rel="stylesheet">
    <link href="../design/css/style-responsive.css" rel="stylesheet" />
    <!-- DINAMIK TABEL -->

	<!-- JS na  -->
	<script type="text/javascript" src="../design/js/jquery.js"></script>
	<script type="text/javascript" src="../design/js/bootstrap.js"></script>
    <script type="text/javascript" src="../design/js/tooltip-viewport.js"></script>
	<!-- JS na  -->

</head>

<body>


<!-- Menu -->
<?php include "menu.php"; ?>
<!-- Menu -->




<!-- ======================================= Content ======================================== -->
<?php include "berita.php";?>
<!-- ======================================= Content ======================================== -->


<script type="text/javascript" language="javascript" src="../design/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../design/js/DT_bootstrap.js"></script>
<script src="../design/js/dynamic_table_init.js"></script>


</body>
</html>