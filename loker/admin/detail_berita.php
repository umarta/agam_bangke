<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="kimiamania" />

	<title>Berita</title>
    
	<link rel="stylesheet" type="text/css" href="../design/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../design/css/style.css">
    <link rel="stylesheet" type="text/css" href="../design/css/font-awesome.css">
    
    <!-- DINAMIK TABEL -->
    <link rel="stylesheet" href="../design/css/demo_page.css" />
    <link rel="stylesheet" href="../design/css/demo_table.css" />
    <link rel="stylesheet" href="../design/css/DT_bootstrap.css" />
    <link href="../design/css/style2.css" rel="stylesheet">
    <link href="../design/css/style-responsive.css" rel="stylesheet" />
    <!-- DINAMIK TABEL -->

	<!-- JS na  -->
	<script type="text/javascript" src="../design/js/jquery.js"></script>
	<script type="text/javascript" src="../design/js/bootstrap.js"></script>
    <script type="text/javascript" src="../design/js/tooltip-viewport.js"></script>
	<!-- JS na  -->
    
    <style>
    </style>

</head>

<body>


<!-- Menu -->
<?php include "menu.php"; ?>
<!-- Menu -->




<!-- ======================================= Content ======================================== -->
<div class="content">
  <div class="row" style="margin: 0;">
      <div class="col-md-3" style="background-color:#333b3a; padding: 0 0 15px 0;">
        <div class="navigasi">          
            <h4>LOKER</h4>
            <a href="berita.php">
                <div class="sub-navigasi aktif"><i class="fa fa-user"></i>&nbsp;&nbsp; Berita
                    <span>
                    <?php
                     include "mesin/koneksi.php";
                     $a = mysqli_query($lnk,"select * from berita");
                     $b = mysqli_num_rows($a);
                     echo $b." data";
                     ?>
                    </span>
                </div>
            </a>
        </div>
      </div>
      <div class="col-md-9" >
      <div>
        <div class="content-isi shadow">
        
            <div class="row" style="margin-top: 10px; margin-bottom: 20px;">
                <div class="col-md-4" style="padding-left: 30px;"><a href="berita.php" class="btn btn-sm btn-primary"><i class="fa fa-arrow-circle-o-left"></i>&nbsp; Kembali</a></div>
                <div class="col-md-4"><h2 style="margin:0;">Detail Data Berita</h2></div>
                <div class="col-md-4" style="padding-right: 30px;">
                    <a href="#" style="float: right;" class="btn btn-ku" data-toggle="modal" data-target=".tambah-data">Tambah Data</a>
                </div>
            </div>
            
            <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">
                    <div class="adv-table table-responsive">
                    <form method="post" action="mesin/mesin_edit_berita.php" enctype="multipart/form-data">
                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                    <thead>
                    <tr>
                        
                    </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                        include "mesin/koneksi.php";
                        $id = $_GET['id'];
                        $sql= mysqli_query($lnk,"select * from berita where id_berita='$id'");
                        $jumlah= mysqli_num_rows($sql);
                        if($jumlah > 0){
                        $i=1;
                        while($array=mysqli_fetch_assoc($sql)){ ?>
                        
                        <tr>
                            <td>Judul</td>
                            <td><input type="text" class="form-control" name="judul" value="<?php echo $array['judul_berita']?>"/></td>
                        </tr>
                        <tr>
                            <td>Gambar</td>
                            <td>
                                <input type="hidden" name="id" value="<?php echo $array['id_berita']?>"/>
                                <input type="hidden" name="oldfoto" value="<?php echo $array['gambar']?>"/>
                                <img src='mesin/uploaded/<?php echo $array['gambar']?>' style="max-width: 200px;height: auto;margin-bottom: 15px;"><br />
                                Unggah foto baru <input type="file" class="form-control" name="newfoto"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Isi</td>
                            <td>
                                <textarea name="isi" class="form-control" style="height: 300px; resize: none;"><?php echo $array['isi_berita']?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                            <div class="btn-group  btn-group-sm">
                                <button type="reset" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i>&nbsp; Atur Ulang</button>
                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i>&nbsp; Sunting</button>
                            </div>
                            </td>
                        </tr>
                    
                        <?php $i++;} }else{ ?>
                             <tr>
                                <td colspan="6">Tidak ada data</td>
                             </tr>
                        <?php }?>
                    
                    </tbody>
                    </table>
                    </form>
                    </div>
                    </div>
                </section>
            </div>
        </div> 
            
            
            
            <!-- Tambah Data Berita -->
            <form method="post" action="mesin/mesin_tambah_berita.php" enctype="multipart/form-data">
            <div class="modal fade tambah-data">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title text-center">Tambah Data Berita</h4>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <td>Judul Berita</td>
                                        <td><input type="text" class="form-control" name="judul" required=""/></td>
                                    </tr>
                                    <tr>
                                        <td>Isi</td>
                                        <td><textarea name="isi" class="form-control" required="" style="resize: none; height: 300px;"></textarea></td>
                                    </tr>
                                    <tr>
                                        <td>Foto</td>
                                        <td><input type="file" name="gambar"/></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Batal</button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp; Tambah Data</button>
                        </div>
                    </div>
                </div>
            </div>
            </form>
            <!-- Tambah Data Siswa -->
            
            
            
        </div>
      </div>  
      </div>
  </div>
</div>
<!-- ======================================= Content ======================================== -->


<script type="text/javascript" language="javascript" src="../design/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../design/js/DT_bootstrap.js"></script>
<script src="../design/js/dynamic_table_init.js"></script>


</body>
</html>