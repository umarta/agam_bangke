<div class="navbar navbar-form navbar-fixed-top" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
      
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#"><i class="fa fa-envelope fa-lg"></i></a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg"></i> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#"><i class="fa fa-user"></i>&nbsp; Profil</a></li>
                <li><a href="#"><i class="fa fa-cog"></i>&nbsp; Pengaturan</a></li>
                <li class="divider"></li>
                <li><a href="logout.php"><i class="fa fa-sign-out"></i>&nbsp; Keluar</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
</div>