<!DOCTYPE HTML>
<html>
<head>

</script>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="kimiamania" />

	<title>Lowongan</title>
    
	<link rel="stylesheet" type="text/css" href="../design/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../design/css/style.css">
    <link rel="stylesheet" type="text/css" href="../design/css/font-awesome.css">
    
    <!-- DINAMIK TABEL -->
    <link rel="stylesheet" href="../design/css/demo_page.css" />
    <link rel="stylesheet" href="../design/css/demo_table.css" />
    <link rel="stylesheet" href="../design/css/DT_bootstrap.css" />
    <link href="../design/css/style2.css" rel="stylesheet">
    <link href="../design/css/style-responsive.css" rel="stylesheet" />
    <!-- DINAMIK TABEL -->

	<!-- JS na  -->
	<script type="text/javascript" src="../design/js/jquery.js"></script>
	<script type="text/javascript" src="../design/js/bootstrap.js"></script>
    <script type="text/javascript" src="../design/js/tooltip-viewport.js"></script>
	<!-- JS na  -->
    
    <style>
    </style>

</head>

<body>


<!-- Menu -->
<?php include "menu.php"; ?>
<!-- Menu -->




<!-- ======================================= Content ======================================== -->
<div class="content">
  <div class="row" style="margin: 0;">
      <div class="col-md-3" style="background-color:#333b3a; padding: 0 0 15px 0;">
        <div class="navigasi">          
            <h4>LOKER</h4>
           
            <a href="berita.php">
                <div class="sub-navigasi aktif"><i class="fa fa-user"></i>&nbsp;&nbsp; Lowongan
                    <span>
                    <?php
                     include "koneksi.php";
                    $a = mysqli_query($lnk,"select*from berita");
                    $b = mysqli_num_rows($a);
                     echo $b." data";
                     ?>
                    </span>
                </div>
            </a>
        </div>
      </div>
      <div class="col-md-9" >
      <div>
        <div class="content-isi shadow">
        
            <div class="row" style="margin-top: 10px; margin-bottom: 20px;">
                <div class="col-md-4"></div>
                <div class="col-md-4"><h2 style="margin:0;">Data Lowongan</h2></div>
                <div class="col-md-4">
                    <a href="#" style="float: right;" class="btn btn-ku" data-toggle="modal" data-target=".tambah-data">Tambah Data</a>
                </div>
            </div>
            
            <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <div class="panel-body">
                    <div class="adv-table table-responsive">
                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Judul</th>
                        <th>Gaji</th>
                        <th>Isi</th>
                        <th>Waktu</th>
                        <th>Gambar</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                        include "mesin/koneksi.php";
                        $sql= mysqli_query($lnk,"select*from berita order by id_berita asc");
                        $jumlah= mysqli_num_rows($sql);
                        if($jumlah > 0){
                        $i=1;
                        while($array=mysqli_fetch_assoc($sql)){ ?>
                    
                    <tr class="gradeX">
                            <td><?php echo $i;?></td>
                            <td><?php echo $array['gaji']?></td>
                            <td><?php echo $array['judul_berita']?></td>

                            <td>
                                <?php  
                                    $isiz = $array['isi_berita'];
                                    echo substr($isiz,0,20)." ...";
                                ?>
                            </td>
                            <td><?php echo $array['waktu']?></td>
                            <td>
                                <img src='/mesin/uploaded/<?php echo $array['gambar']?>' style="max-width: 100px;height: auto;">
                            </td>
                            <td>
                            <div class="btn-group  btn-group-sm">
                                    <a onclick="return confirm('Apakah yakin akan di hapus?')" href="mesin/mesin_hapus_berita.php?id=<?php echo $array['id_berita']?>" class="btn-sm btn btn-danger tooltip-viewport-bottom" type="button" title="Hapus"><i class="fa fa-trash-o"></i></a>
                                    <a href="detail_berita.php?id=<?php echo $array['id_berita']?>" class="btn btn-info btn-sm tooltip-viewport-bottom" type="button" title="Detail"><i class="fa fa-check-square-o"></i></a>
                            </div>    
                            </td>
                    </tr>
                    
                        <?php $i++;} }else{ ?>
                             <tr>
                                <td colspan="6">Tidak ada data</td>
                             </tr>
                        <?php }?>
                    
                    </tbody>
                    </table>
                    </div>
                    </div>
                </section>
            </div>
        </div> 
            
            
            
            <!-- Tambah Data Berita -->
            <form method="post" action="mesin/mesin_tambah_berita.php" enctype="multipart/form-data">
            <div class="modal fade tambah-data">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title text-center">Tambah Data Lowongan</h4>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <td>Judul Lowongan</td>
                                        <td><input type="text" class="form-control" name="judul" required=""/></td>
                                    </tr>
                                    <tr>
                                    <td>Gaji </td>
                                   <td><input type="text" class="form-control" name="gaji" required=""/></td></tr>
                                   <td>Isi </td>

                                        <td><textarea name="isi" class="form-control" required="" style="resize: none; height: 300px;"></textarea></td>
                                         
                                    </tr>
                                    
                                    <tr>
                                        <td>Foto</td>
                                        <td><input type="file" name="gambar"/></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Batal</button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp; Tambah Data</button>
                        </div>
                    </div>
                </div>
            </div>
            </form>
            <!-- Tambah Data Siswa -->
            
            
            
        </div>
      </div>  
      </div>
  </div>
</div>
<!-- ======================================= Content ======================================== -->


<script type="text/javascript" language="javascript" src="../design/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../design/js/DT_bootstrap.js"></script>
<script src="../design/js/dynamic_table_init.js"></script>


</body>
</html>