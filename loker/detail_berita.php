<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="kimiamania" />

	<title>SMK Bakti Nusantara 666 | Terbaru</title>

	<link rel="SHORTCUT ICON" href="../img/logo.png">
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="../bootstrap/css/font-awesome.css"/>
    <link rel="stylesheet" href="../bootstrap/css/style.css"/>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="../bootstrap/css/prettify.css"/>
    <!-- Bootstrap -->

    <!-- Hover Animation -->
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/common.css" />
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/hover.css" />
    <!-- Hover Animation -->
    
    <!-- Flat -->
    <link rel="stylesheet" href="../flat/css/flat-ui.css"/>
    <!-- Flat -->
    
    <!-- JS -->
    <script src="../flat/js/jquery-1.8.3.min.js"></script>
    <script src="../flat/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="../flat/js/jquery.ui.touch-punch.min.js"></script>
    <script src="../flat/js/bootstrap.min.js"></script>
    <script src="../flat/js/bootstrap-select.js"></script>
    <script src="../flat/js/bootstrap-switch.js"></script>
    <script src="../flat/js/flatui-checkbox.js"></script>
    <script src="../flat/js/flatui-radio.js"></script>
    <script src="../flat/js/jquery.tagsinput.js"></script>
    <script src="../flat/js/jquery.placeholder.js"></script>
    <script src="../flat/js/typeahead.js"></script>
    <script src="../bootstrap/js/google-code-prettify/prettify.js"></script>
    <script src="../flat/js/application.js"></script>
    <!-- JS -->
    
</head>

<body>




<!-- ==================================== Menu =========================== -->
<nav class="navbar navbar-ku navbar-embossed" role="navigation" style="border-radius: 0; margin-bottom: 0;">
<div class="container">
  <div class="navbar-header"  style="margin: 0;">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
      <span class="sr-only">Toggle navigation</span>
    </button>
    <a class="navbar-brand" href="index.php">BAKNUS 666</a>
  </div>
  <div class="collapse navbar-collapse" id="navbar-collapse-01">
    <ul class="nav navbar-nav">           
      <li><a href="index.php">Beranda</a></li>
      <li class="active"><a href="terbaru.php">Terbaru</a></li>
      <li><a href="tentang.php">Tentang</a></li>
      <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Penduduk <b class="caret"></b></a>
          <span class="dropdown-arrow"></span>
          <ul class="dropdown-menu" style="padding: 0; border-radius: 0;">
            <li><a href="guru.php" style="border-radius: 0;">Guru</a></li>
            <li><a href="murid.php" style="border-radius: 0;">Murid</a></li>
          </ul>
      </li>
    </ul>
    <ul class="nav navbar-right">          
        <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Pencarian.." style="border-radius: 0;">
            </div>
            <button type="submit" class="btn btn-default btn-embossed" style="border-radius: 0;">Cari</button>
        </form>
    </ul> 
  </div><!-- /.navbar-collapse -->
</div>
</nav><!-- /navbar -->
<!-- ==================================== Menu =========================== -->





<!-- ==================================== Header =========================== -->
<?php include "header.php"; ?>
<!-- ==================================== Header =========================== -->






<!-- ==================================== Content =========================== -->

<!-- konten Bawah -->
<div class="konten2">
    <div class="container">
        <div class="row" style="margin:25px 0 0 0;">
        
        <?php
        include "koneksi.php";
        $id = $_GET['id'];
        $sql = mysqli_query($lnk,"select*from berita where id_berita='$id'");
        $array=mysqli_fetch_assoc($sql)
        ?>
        
        
           
            
            <div class="col-md-8">
                <div class="panel panel-default">
                <div class="panel-body" style="padding: 25px;">
                    <div class="pembuat-berita">
                        <i class="fa fa-calendar"></i>&nbsp;&nbsp; <?php echo $array['waktu']?> &nbsp;&nbsp;&nbsp;&nbsp;
                        <span style="font-weight: bold;"> <i class="fa fa-user"></i> &nbsp; Administrator</span>
                    </div>
                    <div class="judul-berita">
                        <?php echo $array['judul_berita']?>
                    </div>
                    <div style="margin: 15px 0;">
                        <img src="../admin/mesin/uploaded/<?php echo $array['gambar']?>" class="img-responsive" />
                    </div>
                    <div class="isi-berita">
                        <?php echo $array['isi_berita']?>
                    </div>
                </div>
                </div>
            </div>
            
            
            
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body" style="padding-top: 0;padding-bottom: 0;">
                        <h4>Berita Populer</h4>
                    </div>
                </div>
            <?php
            
            $sql = mysqli_query($lnk,"select*from berita order by id_berita desc limit 2");
            while($array=mysqli_fetch_assoc($sql)){
            ?>
            
                <div class="box-isi shadow">
                    <div class="pembuat-berita">
                        <i class="fa fa-calendar"></i>&nbsp;&nbsp; <?php echo $array['waktu']?> &nbsp;&nbsp;&nbsp;&nbsp;
                        <span style="font-weight: bold;"> <i class="fa fa-user"></i> &nbsp; Administrator</span>
                    </div>
                    <div class="img-berita">
                        <img src="../admin/mesin/uploaded/<?php echo $array['gambar']?>" class="img-responsive"/>
                    </div>
                    <div class="judul-berita">
                        <?php echo $array['judul_berita']?>
                    </div>
                    <div class="isi-berita">
                        <?php echo $array['isi_berita']?>
                    </div>
                    <div class="selengkapnya">
                        <a href="detail_berita.php?id=<?php echo $array['id_berita']?>" class="btn btn-info btn-embossed">Lihat Selengkapnya</a>
                    </div>
                </div>
                
                <?php } ?>
                
                
            </div>
            
            
            
           
            
        </div>
    </div>
</div>
<!-- konten Bawah -->

<!-- ==================================== Content =========================== -->





<!-- ========================================== Footer ========================== -->
<?php include "footer.php" ;?>
<!-- ========================================== Footer ========================== -->





</body>
</html>